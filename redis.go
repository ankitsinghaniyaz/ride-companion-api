package main

import (
	"log"

	"github.com/go-redis/redis"
)

// SetupRedis create redis client
func SetupRedis() *redis.Client {
	opt, err := redis.ParseURL(Config.RedisURL)
	if err != nil {
		panic(err)
	}

	opt.DB = 0

	client := redis.NewClient(opt)

	// setup eviction policy on the redis client
	client.ConfigSet("maxmemory", Config.RedisMaxMemory)
	client.ConfigSet("maxmemory-policy", "allkeys-lru")

	_, err = client.Ping().Result()

	if err != nil {
		log.Println("Redis: failed to connect", err)
	} else {
		log.Println("Redis: connected")
	}

	return client
}
