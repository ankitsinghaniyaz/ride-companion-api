package main

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

// EnvConfig - global struct to store the env varilables
type EnvConfig struct {
	Env            string
	Port           string
	RedisURL       string
	RedisMaxMemory string
}

// SetupEnv - configures all environemnt variable and make it accessible via Config
func SetupEnv() EnvConfig {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	// set development if no environment is set
	env := os.Getenv("RIDE_COMPANION_ENV")
	if "" == env {
		env = "development"
	}

	// config object which will contain all the env values
	cfg := EnvConfig{}
	cfg.Env = env
	cfg.Port = os.Getenv("PORT")
	cfg.RedisURL = os.Getenv("REDIS_URL")
	cfg.RedisMaxMemory = os.Getenv("REDIS_MAX_MEMORY")

	return cfg
}
