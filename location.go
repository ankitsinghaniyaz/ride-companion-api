package main

import (
	"encoding/json"
	"log"

	"github.com/go-redis/redis"
)

// Location - stores the lat and lng of the user
type Location struct {
	TravellerID string `json:"traveller_id"`
	Lat         string `json:"lat"`
	Lng         string `json:"lng"`
}

// MarshalBinary -
func (location *Location) MarshalBinary() ([]byte, error) {
	return json.Marshal(location)
}

// UnmarshalBinary -
func (location *Location) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &location); err != nil {
		return err
	}

	return nil
}

// Store - store the value in redis store
func (location *Location) Store(key string) error {
	marshalled, err := location.MarshalBinary()
	if err != nil {
		return err
	}
	return RedisClient.Set(key, marshalled, 0).Err()
}

// Fetch - fetch the value from the redis store
func (location *Location) Fetch(key string) {
	data, err := RedisClient.Get(key).Result()

	if err == redis.Nil {
		log.Printf("No trip found with id: %s", key)
		return
	} else if err != nil {
		panic(err)
	}

	if err := json.Unmarshal([]byte(data), location); err != nil {
		panic(err)
	}
}
