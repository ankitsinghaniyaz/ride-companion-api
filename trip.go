package main

import (
	"encoding/json"
	"log"

	"github.com/go-redis/redis"
)

// Trip structure to hold the information about a trip
type Trip struct {
	// Name       string      `json:"name"`
	ID         string      `json:"id"`
	Travellers []Traveller `json:"travellers"`
}

// MarshalBinary -
func (trip *Trip) MarshalBinary() ([]byte, error) {
	return json.Marshal(trip)
}

// UnmarshalBinary -
func (trip *Trip) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &trip); err != nil {
		return err
	}

	return nil
}

// Store - store the value in redis store
func (trip *Trip) Store(key string) error {
	marshalled, err := trip.MarshalBinary()
	if err != nil {
		return err
	}
	return RedisClient.Set(key, marshalled, 0).Err()
}

// Fetch - fetch the value from the redis store
func (trip *Trip) Fetch(key string) {
	data, err := RedisClient.Get(key).Result()

	if err == redis.Nil {
		log.Printf("No trip found with id: %s", key)
		return
	} else if err != nil {
		panic(err)
	}

	if err := json.Unmarshal([]byte(data), trip); err != nil {
		panic(err)
	}
}

// TravellerKeys - search all travellers keys from the store
func (trip *Trip) TravellerKeys() []string {
	keys, err := RedisClient.Keys(trip.ID + "_*").Result()

	if err == redis.Nil {
		log.Printf("No travellers in trip: %s", trip.ID)
		return nil
	} else if err != nil {
		panic(err)
	}

	return keys
}
