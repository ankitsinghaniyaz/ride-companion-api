# What more needs to be done?

## Android

- on destination serach, show map
- add ability to invite friends to join the trip by link
- show each user on the map

- customize the marker on the map
- share link to the trip
- send live location to the trip every 5 second to the trip
- on location receive update the location on map


## Backend
- authentication using google sso
- create trip
- share link to trip
- ability to add traveller to a trip
- store current location of each user

user#trip: location

# Learn

- pointers
- channels
- struct