package main

// Traveller - is a participant of a trip
type Traveller struct {
	// TripID string `json:"trip_id"`
	ID     string `json:"id"`
	Name   string `json:"name"`
	Avatar string `json:"avatar"`
}
