package main

import (
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

// Logger utility to log network request information
func Logger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		next.ServeHTTP(w, r)

		log.WithFields(log.Fields{
			"method":   r.Method,
			"duration": time.Since(start),
		}).Info(r.RequestURI)
	})
}
