package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	uuid "github.com/nu7hatch/gouuid"
)

// TripCreateHandler initiates and handles the web socket connection
func TripCreateHandler(w http.ResponseWriter, r *http.Request) {
	id := uid()
	var travellers []Traveller
	trip := Trip{ID: id, Travellers: travellers}

	trip.Store(trip.ID)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(trip)
}

// TripJoinHandler initiates and handles the web socket connection
func TripJoinHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	tripID := vars["trip"]

	var trip Trip
	trip.Fetch(tripID)

	// if trip not found
	if trip.ID != tripID {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		response := map[string]string{
			"message": "Trip Not Found",
		}

		json.NewEncoder(w).Encode(response)
		return
	}

	// update travellers
	var travellers []Traveller

	updatingTraveller := Traveller{
		ID:     r.FormValue("id"),
		Name:   r.FormValue("name"),
		Avatar: r.FormValue("avatar"),
	}

	found := false
	for _, traveller := range trip.Travellers {
		if traveller.ID == updatingTraveller.ID {
			found = true
			travellers = append(travellers, updatingTraveller)
		} else {
			travellers = append(travellers, traveller)
		}
	}

	// this is the a new traveller
	if !found {
		travellers = append(travellers, updatingTraveller)
	}

	trip.Travellers = travellers
	trip.Store(trip.ID)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(trip)
}

// TripSyncHandler - syncs user location
func TripSyncHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	vars := mux.Vars(r)
	tripID := vars["trip"]
	var trip Trip
	trip.Fetch(tripID)

	// if trip not found
	if trip.ID != tripID {
		w.WriteHeader(http.StatusNotFound)
		response := map[string]string{
			"message": "Trip Not Found",
		}

		json.NewEncoder(w).Encode(response)
		return
	}

	// if traveller has not joined
	joined := false
	travellerID := r.FormValue("id")
	for _, traveller := range trip.Travellers {
		if traveller.ID == travellerID {
			joined = true
			break
		}
	}

	if !joined {
		w.WriteHeader(http.StatusNotFound)
		response := map[string]string{
			"message": fmt.Sprintf("Traveller %s has not joined the trip %s yet", travellerID, trip.ID),
		}

		json.NewEncoder(w).Encode(response)
		return
	}

	location := Location{
		TravellerID: r.FormValue("id"),
		Lat:         r.FormValue("lat"),
		Lng:         r.FormValue("lng"),
	}

	// store the current location
	key := trip.ID + "_" + location.TravellerID
	location.Store(key)

	// get all the locations for this trip
	var locations []Location
	for _, traveller := range trip.Travellers {
		key = trip.ID + "_" + traveller.ID
		var travellerLocation Location
		travellerLocation.Fetch(key)
		locations = append(locations, travellerLocation)
	}

	json.NewEncoder(w).Encode(locations)
}

// TravellerDeleteHandler - removes a traveller from the trip
func TravellerDeleteHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	vars := mux.Vars(r)
	tripID := vars["trip"]
	travellerID := vars["traveller"]

	var trip Trip
	trip.Fetch(tripID)

	// if trip not found
	if trip.ID != tripID {
		w.WriteHeader(http.StatusNotFound)
		response := map[string]string{
			"message": "Trip Not Found",
		}

		json.NewEncoder(w).Encode(response)
		return
	}

	var travellers []Traveller
	for _, traveller := range trip.Travellers {
		if traveller.ID != travellerID {
			travellers = append(travellers, traveller)
		}
	}

	trip.Travellers = travellers
	trip.Store(trip.ID)

	json.NewEncoder(w).Encode(trip)
}

// helpers
// uid - generates reusable uid
func uid() string {
	uid, err := uuid.NewV4()
	if err != nil {
		log.Panicln("failed to generate uuid")
		panic(err)
	}

	return uid.String()
}
