package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

// Config - stores global environment configurations
var Config = SetupEnv()

// RedisClient - setup redis and create a client
var RedisClient = SetupRedis()

func main() {
	setupLogger()
	router := mux.NewRouter()
	api := router.PathPrefix("/api/v1/").Subrouter()
	api.HandleFunc("/trip", TripCreateHandler).Methods("post")
	api.HandleFunc("/trip/{trip}/join", TripJoinHandler).Methods("post")
	api.HandleFunc("/trip/{trip}/sync", TripSyncHandler).Methods("post")
	api.HandleFunc("/trip/{trip}/traveller/{traveller}", TravellerDeleteHandler).Methods("delete")
	router.Use(Logger)

	// Start the server on localhost port 8000 and log any errors
	log.Printf("starting in env: %s", Config.Env)
	log.Printf("http server started on http://0.0.0.0:%s", Config.Port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", Config.Port), router))
}

func setupLogger() {
	formatter := &logrus.TextFormatter{
		FullTimestamp: true,
	}
	logrus.SetFormatter(formatter)
}
